const mongoose = require('mongoose');
module.exports = function(uri){
    
    mongoose.connect(uri);
    
    process.on('SIGINT', function(){
		mongoose.connection.close(function(){
			console.log('Mongoose! Disconnected by application of the end');
			process.exit(0);
		});
	});
	
	mongoose.connection.on('connected', function(){
		console.log('Mongoose! Connected in '+ uri);
	});
	
	mongoose.connection.on('disconnected', function(){
		console.log('Mongoose! Disconnected of '+ uri);
	});
	
	mongoose.connection.on('error', function(erro){
		console.log('Mongoose! Error at the connection');
	});
}