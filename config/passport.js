'use strict';

const   passport = require('passport'),
        LocalStrategy = require('passport-local').Strategy,
        mongoose = require('mongoose'),
        bcrypt = require('bcrypt'),
        crypto = require('crypto');
        
const configPassport = () => {
    
    let modelUser = mongoose.model('users');
    
    passport.use(new LocalStrategy(
        {usernameField: 'email', 
        passwordField: 'password',
        passReqToCallback : true,
        session: true},
        function(req, username, password, done){
            modelUser.findOne({"auth.email": username}, 
                function(err, user){
                    if (err) {
                        return done(err);
                    }
                    if(!user){
                        return done(null, false, { message: 'Incorrect email'});
                    }
                    
                    if(!bcrypt.compareSync(password, user.auth.password)){
                        return done(null, false, { message: 'Incorrect password'});
                    }
                    
                    return done(null, user);
                
            });
        }
    ));
    
    passport.serializeUser(
        function(user, done){
            let hashToken = crypto.randomBytes(30).toString('hex');
            user.auth.hashToken = hashToken;
            
            let query = { 
                        $set: {
                            "auth.lastAccess": Date.now(),
                            "auth.hashToken": hashToken
                        } 
                    };
                    
        modelUser.findByIdAndUpdate(user._id, query).exec()
            .then(
                (userUpdate) => {
                    done(null, userUpdate.auth.hashToken)
                },
                (error) => {
                    done(null, user.auth.hashToken)
                }
            )
        done(null, user.auth.hashToken);
    });
    
    passport.deserializeUser(
        function(hashToken, done){
            modelUser.findOne({"auth.hashToken": hashToken}).exec()
                .then(
                    (user) => {
                        done(null, user);
                });
    });
    
}

module.exports = configPassport;