'use strict';
const   express = require('express'),
        load = require('express-load'),
        bodyParser = require('body-parser'),
        cookieParser = require('cookie-parser'),
        session = require('express-session'),
        passport = require('passport'),
        flash = require('connect-flash'),
        helmet = require('helmet');

module.exports = () => {
    let app = express();
    
    //app.use(flash());
    app.use(cookieParser());
    app.use(helmet());
	app.use(helmet.hidePoweredBy({ setTo: 'PHP 5.5.14' }));
    app.use(session({
        secret: 'looking for success',
        resave: true,
        saveUninitialized: true
    }));
    app.use(passport.initialize());
    app.use(passport.session());
    app.use(helmet.xssFilter());
	app.use(helmet.noSniff());
	app.use(helmet.frameguard());
    app.use(flash());
    
    app.set('view engine', 'ejs');
	app.set('views', './app/views');
	
	app.use(express.static('./public'));
	app.use(bodyParser.urlencoded({extended: true}));
	app.use(bodyParser.json());
  	app.use(require('method-override')());
    
    load('models', {cwd: 'app'})
        .then('controllers')
        .then('routes')
        .into(app);
    
    app.set('port', 3000);
    return app;
}