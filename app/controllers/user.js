'use strict';

const   util = require('../../app/controllers/util.js')(),
        bcrypt = require('bcrypt'),
        saltRounds = 15;

const userController = (app) => {
    
    let controller = {},
        modelUser = app.models.user,
        modelOrder = app.models.order;
    
    controller.singUp = (req, res) => {
        
        let firstName = req.body.firstName || null,
            lastName = req.body.lastName || null,
            genre = req.body.genre || null,
            avatarPath = req.body.avatarPath || null,
            userName = req.body.userName || null,
            email = req.body.email || null,
            password = req.body.password || null,
            disabled = req.body.disabled || null;
            
        if(!util.isValid(firstName)){
            res.status(400).json({'serverMessage': 'Invalid name'});
            return;
        }
        if(!util.isValid(lastName)){
            res.status(400).json({'serverMessage': 'Invalid name'});
            return;
        }
        if(!util.isValid(userName)){
            res.status(400).json({'serverMessage': 'Invalid username'});
            return;
        }
        if(!util.isEmail(email)){
            res.status(400).json({'serverMessage': 'Invalid email'});
            return;
        }
        if(!util.isValid(password)){
            res.status(400).json({'serverMessage': 'Invalid password'});
            return;
        }
        
        let salt = bcrypt.genSaltSync(saltRounds),
            hash = bcrypt.hashSync(password, salt);
        
        
        let data = {
            firstName : firstName,
            lastName: lastName,
            genre: genre,
            avatarPath: avatarPath,
            auth:{
                userName: userName,
                email: email,
                password: hash
            }
        };
        
        modelUser.create(data)
            .then(
                (user) => {
                    if(req.query.order){
                        let order = app.controllers.order;
                        order.updateUserToOrder(req, res, (err, dataOrder) => {
                            res.status(201).json({'serverMessage': 'Login with success'});
                        });
                        return;
                    }
                    res.status(201).json({'serverMessage': 'Login with success'});
                },
                (error) => {
                    if (11000 === error.code || 11001 === error.code) {
                        return res.status(409).json({'serverMessage': 'Already register'});
                    }
                    res.status(500).json({'serverMessage': 'Internal server error'});
                }
            )
            
            
    };
    
    controller.order = (req, res) => {
        
        modelOrder.find({"buyer.idUser": req.user._id},{_id: true, "payment.status": true, total: true}, {sort:{_id: -1}}).exec()
            .then(
                (order) => {
                    res.status(200).json(order);
                },
                (error) => {
                    res.status(404).json({'serverMessage': 'Not found order'});
                }
            )
    }

    controller.orderDetails = (req, res) => {

        modelOrder.find({_id: req.params.salesId, "buyer.idUser": req.user._id}).exec()
            .then(
                (order) => {
                    if(order.length > 0){
                        res.status(200).json(order);
                        return;
                    }
                    res.status(200).json({serverMessage: 'Nenhum pedido'});
                }
            )

    };
    
    controller.perfil = (req, res) => {
        let user = {
            'firstName' : req.user.firstName,
            'lastName': req.user.lastName,
            'email': req.user.auth.email 
        }
        res.status(200).json(user);
    }
    
    controller.changePassword = (req, res) => {
        
        let password = req.body.password || null,
            newPassword = req.body.newPassword || null,
            repeatNewPassword = req.body.repeatNewPassword || null;
            
        if(!util.isValid(password) || password.length < 8){
            res.status(400).json({'serverMessage': 'Password is not null'});
            return;
        }
        if(!util.isValid(newPassword) || newPassword.length < 8){
            res.status(400).json({'serverMessage': 'New Password is not null'});
            return;
        }
        if(!util.isValid(repeatNewPassword) || repeatNewPassword.length < 8){
            res.status(400).json({'serverMessage': 'Repeat New Password is not null'});
            return;
        }
        
        if(newPassword !== repeatNewPassword){
            res.status(400).json({'serverMessage': 'New Password  and repeat password is must to be iqual'});
            return;
        }
        
        if(!bcrypt.compareSync(password, req.user.auth.password)){
            res.status(409).json({'serverMessage': 'Password invalid'});
            return;
        }
        
        let salt = bcrypt.genSaltSync(saltRounds);
        newPassword = bcrypt.hashSync(newPassword, salt);
        let query = {$set:{'auth.password': newPassword}};
        modelUser.update({_id: req.user._id}, query).exec()
            .then(
                (user) => {
                    console.log(user);
                    if(parseInt(user.nModified) > 0){
                        req.user.auth.password = newPassword;
                        res.status(200).json({'serverMessage': 'Changed password with success'});
                        return;
                    }
                    res.status(304).json({'serverMessage': 'Not possible change password'});
                },
                (error) => {
                    res.status(500).json({'serverMessage': 'Internal server error'});
                }
            )
        
    }
    
    return controller;
}

module.exports = userController;