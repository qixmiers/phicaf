'use strict';
const   Mailgun = require('mailgun').Mailgun,
        mailcomposer = require("mailcomposer"),
        mg = new Mailgun('key-8832bec13c58e17b70f878ec1424f3d5'),
        util = require('../../app/controllers/util.js')();
        
const controllerMail = (app) => {
    let controller = {},
        modelEmail = app.models.mail;
    
    controller.sendText = (mailObject) => {
        
        //mg.sendRaw(mailObject.from, [mailObject.to, 'https://phicafe.com.br'], mail, function(err) { err && console.log(err) });
        
        var text = ''
        
        sendText(mailObject.from, [mailObject.to, 'https://phicafe.com.br'], mailObject.subject, mailObject.message, [servername=''], [options={}], [callback(err)])
        /*
        mg.sendRaw(mailObject.from,
        [mailObject.to, 'https://phicafe.com.br'],
        'From: ' + mailObject.from +
          '\nTo: ' + mailObject.to +', https://phicafe.com.br' +
          '\nContent-Type: text/html; charset=utf-8' +
          '\nSubject: ' + mailObject.subject+
          '\n\n' + mailObject.message,
        function(err) { err && console.log(err) });*/
        
    }
    
    
    controller.registerMail = (req, res) => {
        
        let email = req.body.email || null;
        
        if(!util.isEmail(email)){
            res.status(400).json({'serverMessage': 'Invalid Email'});
            return;
        }
        
        let data = {email: email};
        modelEmail.create(data)
            .then(
                (email) => {
                    res.status(201).json({'serverMessage': 'Created with success'});
                },
                (error) => {
                    
                    if(error.code === 11000){
                        return res.status(409).json({'serverMessage': 'Email already registered'});
                    }
                    
                    res.status(500).json({'serverMessage': 'Internal server error'});
                }
            )
        
    }
    
    return controller;
}

module.exports = controllerMail;