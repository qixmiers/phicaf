'use strict';
    
const   Correios = require('node-correios'),
        util = require('../../app/controllers/util.js')(),
        correios = new Correios(),
        cepFrom = 13073002,
        typePackage = 1,
        codeService = '40010,41106,40215', // box or package
                           //40010  //41106  //40215
        codeDescription = ['SEDEX', 'PAC', 'SEDEX 10'],
        request = require('request'),
        originCep = '13073002';
            

let weightTotal = 0;
let createPackage = (product) => {
    weightTotal += (product.weight * product.quantity)
    
}

       
module.exports = (app) => {
    
    let controller = {};
    
    controller.getPrice = (req, res) => {
        let args = {},
            cepTo = req.params.cepTo || null,
            order = req.body.order || null,
            weight = 0,
            length = req.query.lengthPackage || 40,
            height = req.query.heightPackage || 15,
            width = req.query.widthPackage || 15,
            diameter = length+height+width;
            //&weightPackage=20&lengthPackage=20&heightPackage=4&widthPackage=11&diameterPackage=20
        
        if(!util.isValid(cepTo)){
            res.status(400).json({'serverMessage': 'To cep is not null'});
            return;
        }
        
        if(!util.isValid(order)){
            res.status(400).json({'serverMessage': 'Order is not null'});
            return;
        }
        
        order.forEach(function(product){
            weight += (product.weight * product.quantity);
        });
        
        
        if(!util.isValid(length)){
            res.status(400).json({'serverMessage': 'Length is not null'});
            return;
        }
        if(!util.isValid(height)){
            res.status(400).json({'serverMessage': 'Height is not null'});
            return;
        }
        if(!util.isValid(width)){
            res.status(400).json({'serverMessage': 'Width is not null'});
            return;
        }
        if(!util.isValid(diameter)){
            res.status(400).json({'serverMessage': 'Diameter is not null'});
            return;
        }
       
     
        args = {
            nCdServico: codeService,
            sCepOrigem: cepFrom,
            sCepDestino: cepTo,
            nVlPeso: weight,
            nCdFormato: typePackage,
            nVlComprimento: length,
            nVlAltura: height,
            nVlLargura: width,
            nVlDiametro: diameter
        };
        console.log(args);
        correios.consultaCEP({ cep: cepTo}, function(err, result) {
            if(result.erro){
                res.status(404).json({'serverMessage': 'Cep not found'});
                return;
            }
           let urlPageSeguro = 'https://pagseguro.uol.com.br/desenvolvedor/simulador_de_frete_calcular.jhtml?postalCodeFrom='+originCep+'&weight='+weight+'&value=0,00&postalCodeTo='+cepTo;
            request(urlPageSeguro, function (error, response, body) {
                if (!error && response.statusCode == 200) {
                    res.status(200).json(body.split('|'));
                    return;
                }
                res.status(500).json({'serverMessage': 'Internal server error'});
            });
            
            /*
            correios.calcPrecoPrazo(args, function(err, result){
                if(err){
                    res.status(500).json({'serverMessage': 'Internal server error'});
                    return;
                }
                let typeShipping = [];
                result.forEach(function(elem){
                    if(elem.Codigo == 40010){
                        elem.Description = codeDescription[0];
                    }else
                        if(elem.Codigo == 41106){
                            elem.Description = codeDescription[1];
                    }
                    else{
                        elem.Description = codeDescription[2];
                    }
                    elem.Valor = elem.Valor.replace(',','.');
                    typeShipping.push(elem);
                });
                
                res.status(200).json(typeShipping);
            });
            */
        });
        
    };
    
    controller.getAddress = (req, res) => {
        
        let cep = req.params.cepTo || null;
        if(!util.isValid(cep)){
            return res.status(400).json({'serverMessage': 'Cep is not null'});
        }
        correios.consultaCEP({ cep: cep }, function(err, result) {
            if(err || result.erro){
                return res.status(404).json({'serverMessage': err});
            }
            res.status(200).json(result);
        });
        
    }
    
    
    
    return controller;
}