'use strict';
const   util = require('../../app/controllers/util.js')(),
        fs = require('fs');

module.exports = (app) => {
    let modelProduct = app.models.product;
    
    let controller = {};
    
    controller.create = (req, res) => {
        let name = req.body.name || null,
            url = req.body.url || null,
            description = req.body.description || null,
            price = req.body.price || null,
            quantity = req.body.quantity || null,
            productType = req.body.productType || null,
            taste = req.body.taste || null,
            foodPairing = req.body.foodPairing || null,
            award = req.body.award || null,
            video = req.body.video || null,
            avatar = null,
            classHtml = req.body.classHtml || null;
            
        if(req.files.avatar){
            avatar = req.files.avatar[0];
        }
        
        if(req.body.havePermission !== "RICARDO"){
            res.status(401).json('Não autorizado');
            return;
        }
        if(!util.isValid(name)){
            res.status(400).json({'serverMessage': 'Name is not null'});
            return;
        }
        if(!util.isValid(url)){
            res.status(400).json({'serverMessage': 'Url is not null'});
            return;
        }
        if(!util.isValid(description)){
            res.status(400).json({'serverMessage': 'Description is not null'});
            return;
        }
        if(!util.isValid(price)){
            res.status(400).json({'serverMessage': 'Price is not null'});
            return;
        }
        if(!util.isValid(quantity)){
            res.status(400).json({'serverMessage': 'Quantity is not null'});
            return;
        }
        if(!util.isValid(productType)){
            res.status(400).json({'serverMessage': 'Product type is not null'});
            return;
        }
        if(!util.isValid(classHtml)){
            res.status(400).json({'serverMessage': 'Class html is not null'});
            return;
        }
        
        if(!util.isValid(avatar)){
            res.status(400).json({'serverMessage': 'Avatar is not null'});
            return;
        }

        let awards = [];
        
        if(util.isValid(award)){
            award.forEach(function(elem, index){
                awards.push({
                    name: elem.name,
                    avatar: req.files.award[index].filename
                });
            });
        }
        let prices = [];
        price.forEach(function (elem, index) {
            prices.push({
                weight: elem.weight,
                price: elem.price,
                description: elem.description
            });
        });
        
        let data = {
            'name': name,
            'url': url,
            'description': description,
            'prices': prices,
            'quantity': quantity,
            'productType': productType,
            'taste': taste,
            'foodPairing': foodPairing,
            'award': awards,
            'video': video,
            'classHtml': classHtml,
            'avatar': avatar.filename
        };
        
        modelProduct.create(data)
            .then(
                (product) => {
                    res.status(201).json({'serverMessage': 'Create product with success'})
                },
                (error) => {
                    res.status(500).json(error);
                }
            );
        
    }

    controller.productUpdate = (req, res) => {

        let _productId = req.params.idProduct,
            name = req.body.name || null,
            url = req.body.url || null,
            description = req.body.description || null,
            price = req.body.price || null,
            quantity = req.body.quantity || null,
            productType = req.body.productType || null,
            taste = req.body.taste || null,
            foodPairing = req.body.foodPairing || null,
            award = req.body.award || null,
            video = req.body.video || null,
            avatar = null,
            classHtml = req.body.classHtml || null
            setQuery = {};

        if (req.body.havePermission !== 'RICARDO') {
            res.status(401).json('Não autorizado');
            return;
        }

        if (!util.isValid(_productId)) {
            res.status(400).json({'serverMessage': 'productId is not null'});
            return;
        }

        if (name) {
            setQuery.name = name;
        }

        if (url) {
            setQuery.url = url;
        }

        if (description) {
            setQuery.description = description;
        }

        if (price) {
            let prices = [];
            price.forEach(function (elem, index) {
                prices.push({
                    weight: elem.weight,
                    price: elem.price
                });
            });
            setQuery.prices = prices;
        }

        if (quantity) {
            setQuery.quantity = quantity;
        }

        if (productType) {
            setQuery.productType = productType;
        }

        if (taste) {
            setQuery.taste = taste;
        }

        if (foodPairing) {
            setQuery.foodPairing = foodPairing;
        }
        
        if (util.isValid(award)) {
            setQuery.awards = [];
            award.forEach(function(elem, index){
                setQuery.awards.push({
                    name: elem.name,
                    avatar: req.files.award[index].filename
                });
            });
        }

        if (video) {
            setQuery.video = video;
        }

        if (req.files.avatar) {
            setQuery.avatar = req.files.avatar[0].filename;
        }

        if (classHtml) {
            setQuery.classHtml = classHtml;
        }

        if (weight) {
            setQuery.weight = weight;
        }

        var mod = { $set: setQuery };

       // if (setQuery.awards.length > 0 && !setQuery.name) {
         //   mod = { $addToSet: setQuery };
       // }

        modelProduct.update({ _id: _productId }, mod)
            .then( (product) => {
                if (product.nModified > 0) {
                    res.status(200).json({ 'serverMessage': 'Changed product with success' });
                    return;
                }
                res.status(304).json({ 'serverMessage': 'Not possible change product' });
            },
            (error) => {
                res.status(500).json({'serverMessage': 'Internal server error'});
            });
    }
    
    controller.productList = (req, res) => {
        
        modelProduct.find({}, { order: 0 }, { sort: { name: 1, weight: 1 } }).exec()
            .then(
                (products) => {
                    //products.sort(function(a, b){return 0.5 - Math.random()});
                    res.json(products)
                },
                (error) => {
                    res.status(500).json(error);
                }
            )
    }
    
    controller.getProduct = (req, res) => {

        if(!util.isString(req.params.idProduct)){
            res.status(400).json({'serverMessage': 'Url invaled'});
            return;
        }
        var url = {'url': req.params.idProduct};
        modelProduct.findOne(url,{order: 0}).exec()
            .then(
                (product) => {
                    if(!product) throw new Error('Product not found');
                    res.json(product);
                },
                (error) => {
                    res.status(404).json(error);
                }
            )
    }
    
    return controller;
}