'use strict';
const crypto = require('crypto');

const controllerAuth = (app) => {
    
    let controller = {},
        modelUser = app.models.user;
        
    controller.session = (req, res) => {
        var login = '';
        if(req.user) {
            login = req.user.firstName;
        }
        if(req.query.order){
            let order = app.controllers.order;
            order.updateUserToOrder(req, res, (err, dataOrder) => {});
        }
        res.status(200).json({'serverMessage': 'Login with success', 'userName': login});
    };
    
    controller.logout = (req, res) => {
        req.logOut(); // exposto pelo passport
        res.redirect('/');
    }
    
    return controller;
}

module.exports = controllerAuth;