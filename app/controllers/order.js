'use strict';
const   util = require('../../app/controllers/util.js')(),
        statusOrder = ['Aguardando','Aprovado','Cancelado', 'Concluído'],
        pagseguro = require('pagseguro'),
        pagConfig = new pagseguro({
            email : 'ricardopereira@phicafe.com.br',
            //token: '8D10FC9352B2400380537D8FC216AB89',//Sandbox
            //token: '94E996B13AAA4BC9BC3B424FE8C65F8A'//Conta Ricardo
            token: 'CE8212A205CD4E7885AA011502A7CE22' //Phicafe
            //mode : 'sandbox'
        }),
        transform = require( "ee-xml-to-json" );

const controllerOrder = (app) => {
    let controller = {},
        modelProduct = app.models.product,
        modelOrder = app.models.order;
    
    controller.checkCart = (req, res) => {
        
        let products = req.body.products || null;
        
        if(!util.isValid(products) || typeof products !== 'object'){
            res.status(400).json({'serverMessage': 'Product is not null'});
            return;
        }
        
        let listProductID = [];
        products.forEach(function(product){
            listProductID.push(product._id);
        });
        
        let queryProduct =  { _id: {$in : listProductID } };
        modelProduct.find(queryProduct).exec()
            .then(
                (productOrder) => {
                    if(!productOrder) {
                        res.status(404).json({serverMessage: 'Error'});
                        return;
                    }
                    
                    createOrder(
                        (orderModel) => {
                            modelOrder.create(orderModel)
                                .then(
                                    (order) => {
                                        res.status(201).json({'isAuthenticated': req.isAuthenticated(), '_id': order._id});
                                    },
                                    (error) => {
                                        res.status(500).json(error);
                                    }
                                );            
                        }
                    );
                    
                },
                (error) => {
                    res.status(404).json(error);
                }
            );
        
        let createOrder = function(callback){
            let orderModel = {},
                listProduct = [],
                totalProduct = 0;
            
            products.forEach(function(product){
                listProduct.push({
                    idProduct: product._id,
                    productName: product.name,
                    url: product.url,
                    avatar: product.avatar,
                    quantity: product.quantity,
                    subTotal: (product.quantity*product.price),
                    weight: product.weight,
                    typeProduct: product.typeProduct
                });
                totalProduct += (product.quantity*product.price);
            });
            
            orderModel = {
                product: listProduct,
                payment: {
                    name: 'PagSeguro',
                    status: statusOrder[0] 
                },
                total: totalProduct
            };
            
            if(req.isAuthenticated()){
                orderModel.idUser = req.user._id;
            }
            
            return callback(orderModel);
        }
    };
    
    
    controller.pagSeguro = (req, res) => {
        
        let cep = req.body.cep || null,
            street = req.body.street || null,
            homeNumber = req.body.homeNumber || null,
            complement = req.body.complement || null,
            district = req.body.district || null,
            nameBuyer = req.body.nameBuyer || null,
            emailBuyer = req.body.emailBuyer || null,
            phoneAreaCode = req.body.phoneAreaCode || null,
            phoneNumber = req.body.phoneNumber || null,
            city = req.body.city || null,
            state = req.body.state || null,
            order = req.params.idOrder;
            
           if(!util.isValid(cep)){
                return res.status(400).json({'serverMessage': 'Cep is not null'});
            }
            if(!util.isValid(street)){
                return res.status(400).json({'serverMessage': 'Street is not null'});
            }
            if(!util.isValid(homeNumber)){
                return res.status(400).json({'serverMessage': 'Number is not null'});
            }
            
            if(!util.isValid(district)){
                return res.status(400).json({'serverMessage': 'District is not null'});
            }
            if(!util.isValid(city)){
                return res.status(400).json({'serverMessage': 'City is not null'});
            }
            if(!util.isValid(state)){
                return res.status(400).json({'serverMessage': 'State is not null'});
            }
            if(!util.isValid(nameBuyer)){
                return res.status(400).json({'serverMessage': 'Name is not null'});
            }
            if(!util.isValid(emailBuyer)){
                return res.status(400).json({'serverMessage': 'Email is not null'});
            }
            if(!util.isValid(phoneAreaCode)){
                return res.status(400).json({'serverMessage': 'Phone Area is not null'});
            }
            if(!util.isValid(phoneNumber)){
                return res.status(400).json({'serverMessage': 'Phone Number is not null'});
            }
            
            pagConfig.currency('BRL');
            pagConfig.reference(order);
            
            let query = {_id: order, "buyer.idUser": req.user._id};
            modelOrder.findOne(query).exec()
                .then(
                    (order) => {
                        if(!order){
                            res.status(404).json({'serverMessage': 'Not found'});
                            return;
                        }
                        order.product.forEach(function(product, idProduct){
                            var subTotal = product.subTotal;
                            if(product.quantity > 1){
                                subTotal =  (subTotal/product.quantity);
                            }
                            subTotal = subTotal.toString();
                            let price = subTotal.split('.');
                            
                            if(price.length === 1){
                                subTotal = subTotal+'.00';
                            }else if(price[1].length === 1){
                                subTotal = subTotal+'0';
                            }
                                
                            if(!product.weight){
                                product.weight = 500;
                            }
                            pagConfig.addItem({
                                id: (idProduct+1),
                                description: product.productName,
                                amount: subTotal,
                                quantity: product.quantity,
                                weight: product.weight,
                            });
                        });
                        pagConfig.buyer({
                            name: nameBuyer,
                            email: emailBuyer,
                            phoneAreaCode: phoneAreaCode,
                            phoneNumber: phoneNumber
                        });
                        
                        pagConfig.shipping({
                            type: 3,
                            street: street,
                            number: homeNumber,
                            complement: complement,
                            district: district,
                            postalCode: cep,
                            city: city,
                            state: state,
                            country: 'BRA'
                        });
                        
                        pagConfig.setRedirectURL("https://www.phicafe.com.br/#/user/pedido");
                        //pagConfig.setNotificationURL("https://www.phicafe.com.br/pagseguro/notificacao");
                        pagConfig.send(function(err, responsePagSeguro){
                            console.log(responsePagSeguro)
                            if(err || responsePagSeguro == 'Unauthorized'){
                                transform( err, function( err, jsonObj ){
                                    
                                    res.status(404).json(err);
                                });
                                return;
                            }
                            transform( responsePagSeguro, function( err, jsonObj ){
                                
                                if(!jsonObj.checkout){
                                    res.status(500).json({'serverMessage': 'Internal error'});
                                    return;
                                }
                                    let paymentOrder = {
                                        $set:{
                                            "payment.code": jsonObj.checkout.code[0]
                                        }
                                    };
                                
                                modelOrder.update(query, paymentOrder).exec()
                                    .then(
                                        (order) => {
                                            res.status(200).json(jsonObj);
                                        },
                                        (err) => {
                                            res.status(500).json({'serverMessage': 'Internal error'});
                                        }
                                    )
                                
                            });
                        });
                    },
                    (error) => {
                        res.status(500).json({'serverMessage': 'Internal error'});
                    }
                )
            
    }
    
    controller.updateUserToOrder = (req, res, callback) => {
        
        let idUser = req.user._id || null,
            name = req.user.firstName || null,
            order = req.query.order || null,
            isCallback = false;
        
        if(typeof callback === 'function'){
            isCallback = true;
        }
        if(!util.isValid(idUser)){
            if(!isCallback)
                return res.status(400).json({'serverMessage': 'ID user is not null'});
            
            return callback(400, null);
        }
        if(!util.isValid(order)){
            if(!isCallback)
                return res.status(400).json({'serverMessage': 'Order is not null'});
            
            return callback(400, null);
        }
        
        if(name && req.user.lastName){
            name += ' '+req.user.lastName;
        }

        let set = { $set: { "buyer.idUser": idUser, "buyer.name": name }};
        
        modelOrder.update({ _id: order, "buyer.idUser": {$exists: false} }, set).exec()
            .then(
                (order) => {
                    if(typeof callback === 'function'){
                        return callback(null, order);
                    }
                    res.status(200).json(order);
                },
                (error) => {
                    if(typeof callback === 'function'){
                        return callback(error. null);
                    }
                    res.status(304).json(error);
                }
            )
        
    };
    
    controller.isOrder = (req, res) => {
        
        let idUser = req.user._id || null,
            order = req.params.idOrder || null;
            
        if(!util.isValid(order)){
            return res.status(400).json({'serverMessage': 'Order is not null'});
        }
        
        let query = {_id: order, "buyer.idUser": idUser};
        
        modelOrder.findOne(query).exec()
            .then(
                (order) => {
                    if(!order){
                        res.status(404).json({'serverMessage': 'Not found'});
                        return;
                    }
                    if(order.payment.code) {
                        res.status(401).json({'serverMessage': 'Access unauthorized'});
                        return;
                    }
                    let userOrder = {
                        name: req.user.firstName +' '+req.user.lastName,
                        email: req.user.auth.email,
                        order: order._id
                    };
                    
                    res.status(200).json(userOrder);
                },
                (error) => {
                    res.status(401).json({'serverMessage': 'Access unauthorized'});
                }
            )
        
    }
    
    /*
    controller.notification = (req, res) => {
        let notificationCode = req.body.notificationCode || null;
        if(!util.isValid(notificationCode)){
            res.status(400).json({'serverMessage': 'Notification is not null'})
        }
        
        
        
    };
    */
    return controller;
}

module.exports = controllerOrder;