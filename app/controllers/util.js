module.exports = function(app){
	var controller = {};
	
	controller.isValid = function(field){
		if(field === undefined || field === null){
			return false;
		}
		if(typeof field === "string") {
			return field.length > 0;
		}
		return true;
	},
	
	controller.isString = function(value){
		if(value === (undefined || null)){
			return false;
		}
		if(typeof value === 'string'){
			return true;
		}
		return false;
	},
	
	controller.isEmail = function(email){
		var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
		return re.test(email);
	},
	
	controller.isNumber = function(number){
		return /^\d+$/.test(number);
	},
    
	controller.ifExistEmail = function(User, email, callback){
		User.findOne({ email: email }, 'email', function(err, email) {
			return callback(err, email);
		});
	},
    
	controller.dateTimestamp = function(date){
		return new Date(date) / 1000;
	},
	
	controller.dateNow = function(){
		return Math.floor(Date.now()/1000);
	}
	
	return controller;
}