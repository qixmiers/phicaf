'use strict';
const util = require('../../app/controllers/util.js')();

module.exports = (app) => {
    let modelSupport = app.models.support;
    let mailController = app.controllers.mail;
    
    let controller = {};
    
    controller.save = (req, res) => {
        let name = req.body.name || null,
            email = req.body.email || null,
            subject = req.body.subject || null,
            message = req.body.message || null;
            
        if(!util.isValid(name)){
            res.status(400).json({Servermessage: 'Name is not null'});
            return;
        }
        if(!util.isEmail(email)){
            res.status(400).json({Servermessage: 'Email invalid'});
            return;
        }
        if(!util.isValid(subject)){
            res.status(400).json({Servermessage: 'Subject invalid'});
            return;
        }
        if(!util.isValid(message)){
            res.status(400).json({Servermessage: 'Message invalid'});
            return;
        }
        
        let data = {
            'name' : name,
            'email': email,
            'subject' : subject,
            'message': message
        };
        
        modelSupport.create(data)
            .then(
                (support) => {
                    let objectMail = {
                        from: email,
                        to: 'suporte@phicafe.com.br',
                        subject: 'Suporte',
                        message: '<h4>Nome:'+name+'</h4></br>'+
                        ' - Email:'+email+
                        ' - Assunto: '+subject +
                        ' - Message:'+message
                    }
                    mailController.sendText(objectMail);
                    res.status(201).json({Servermessage: 'Send with success'});
                },
                (error) => {
                    res.status(500).json(error);
                }
            );
    };
    return controller;
}