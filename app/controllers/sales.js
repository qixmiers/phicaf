'use strict';

const controllerSales = (app) => {
    let controller = {},
    modelOrder = app.models.order;

    controller.allSales = (req, res) => {
        
        modelOrder.find({"payment.name": {$exists: true}},{_id: 1, dateOrder: 1, total: 1, 'buyer.name': 1}, {sort:{_id: -1}}).exec()
            .then(
                (order) => {
                    if(order.length > 0){
                        res.status(200).json(order);
                        return;
                    }
                    res.status(200).json({serverMessage: 'Nenhum pedido'});
                }
            )
    };

    controller.details = (req, res) => {

        modelOrder.find({_id: req.params.salesId}).exec()
            .then(
                (order) => {
                    if(order.length > 0){
                        res.status(200).json(order);
                        return;
                    }
                    res.status(200).json({serverMessage: 'Nenhum pedido'});
                }
            )

    };

    return controller;
}

module.exports = controllerSales;