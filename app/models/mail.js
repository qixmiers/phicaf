'use strict';
const   mongoose = require('mongoose'),
        crypto = require('crypto');

module.exports = () => {
    
    let schema = mongoose.Schema({
        email: {
            type: String,
            required: true,
            index: {
                unique: true
            }
        },
        dateRegister:{
            type: Date,
            default: Date.now
        },
        hashToken: {
            type: String,
            required: true,
            default: crypto.randomBytes(30).toString('hex')
        },
        validateMail:{
            type: Boolean,
            required: true,
            default: false
        }
    });
    
    return mongoose.model('mail', schema);    
}