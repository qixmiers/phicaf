'use strict';
const mongoose = require('mongoose');
module.exports = () => {
    
    let schema = mongoose.Schema({
        name:{
            type: String,
            required: true,
        },
        url:{
            type: String,
            required: true
        },
        avatar:{
            type: String,
            required: true
        },
        description:{
            type: String,
            required: true,
        },
        prices:[{
            weight:{
                type: String,
                required: true
            },
            price:{
                type: Number,
                required: true
            },
            description: {
                type: String,
                required: true
            }
        }],
        quantity:{
            type: Number,
            required: true
        },
        productType:{
            type: String,
            required: true
        },
        taste:{
            type: String
        },
        foodPairing:{
            type: String
        },
        award:[{
            name: {
                type: String
            },
            avatar:{
                type: String
            }
        }],
        video:{
            type: String
        },
        order:{
            type: [mongoose.Schema.ObjectId],
            ref: 'order',
            required: false
         },
        classHtml:{
            type: String,
            required: true
        }
    });
    
    return mongoose.model('product', schema);
}