'use strict';
const mongoose = require('mongoose');
module.exports = () => {
    
    let schema = mongoose.Schema({
        buyer: {
            idUser:{
                type: mongoose.Schema.ObjectId,
                ref: 'user',
                required: false
            },
            name:{
                type: String,
                required: false
            }
        },
        product:[{
            idProduct:{
                type: mongoose.Schema.ObjectId,
                ref: 'product',
                required: true
            },
            productName:{
                type: String,
                required: true
            },
            url:{
                type: String,
                required: true
            },
            avatar:{
                type: String,
                required: true
            },
            quantity:{
                type: Number,
                required: true
            },
            weight:{
                type: Number,
                required: false  
            },
            typeProduct: {
                type: String,
                required: false
            },
            subTotal:{
                type: Number,
                required: true
            }
        }],
        idGatewayPayment:{
            type: String,
            required: false
        },
        payment:{
            name:{
                type: String,
                required: false
            },
            code:{
                type: String,
                required: false
            },
            notification:{
                type: String,
                required: false
            },
            status:{
                type: String,
                required: false
            },
            reference: {
                type: String,
                required: false
            }
        },
        addressOrder:{
            postalCode:{
                type: String,
                required: false
            },
            street:{
                type: String,
                required: false
            },
            neighborhood:{
                type: String,
                required: false
            },
            city:{
                type: String,
                required: false
            },
            complement:{
                type: String,
                required: false
            }
        },
        total:{
            type: Number,
            required: false
        },
        priceShipping:{
            type: Number,
            required: false
        },
        dateOrder:{
            type: Date,
            default: Date.now
        }
        
    });
    
    return mongoose.model('order', schema);
}