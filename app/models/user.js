'use strict';
const mongoose = require('mongoose'),
      crypto = require('crypto');

module.exports = () => {

let schema = mongoose.Schema({
firstName:{
type: String,
required: true
},
lastName:{
type: String,
required: true
},
genre: {
type: String,
required: false
},
avatarPath: {
type: String,
required: false
},
auth:{
userName: {
type: String,
required: true,
index: {
unique: true
}
},
email: {
type: String,
required: true,
index: {
unique: true
}
},
password:{
type: String,
required: true
},
lastAccess: {
type: Date,
required: false
},
disabled: {
type: Boolean,
required: false
},
hashToken: {
type: String,
required: false,
default: crypto.randomBytes(30).toString('hex')
}
},
dateRegister: {
type: Date,
default: Date.now
},
order:{
type: [mongoose.Schema.ObjectId],
ref: 'orders',
required: false
},
disabledEmail: {
type: Boolean,
required: false,
default: false
}

});

return mongoose.model('users', schema);
}