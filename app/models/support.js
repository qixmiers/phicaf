'use strict';
const mongoose = require('mongoose');
module.exports = () => {
    
    let schema = mongoose.Schema({
        name:{
            type: String,
            required: true
        },
        email:{
            type: String,
            required: true
        },
        subject:{
            type: String,
            require: true
        },
        message:{
           type: String,
            required: true
        }
    });
    return mongoose.model('support', schema);
}