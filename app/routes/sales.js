'use strict';

function verificaAutenticacao(req, res, next) {
    if (req.isAuthenticated() && req.user.auth.email == 'rialexsandro@gmail.com') {
        return next();
    } else {
        res.status(401).json({'serverMessage': 'Access unauthorized', order: req.params.idOrder});
    }
}

const routeSales = (app) => {

    let controller = app.controllers.sales;

    app.route('/sales/all')
        .get(verificaAutenticacao, controller.allSales);
    
    app.route('/sales/:salesId')
        .get(verificaAutenticacao, controller.details);
}

module.exports = routeSales;