'use strict';

const routeIndex = (app) => {
    
    app.get('/', (req, res) => {
        var login = '';
        if(req.user) {
            login = req.user.firstName;
        }
        res.render('index', { userName: login });
    });
    
}

module.exports = routeIndex;