'use strict';

function verificaAutenticacao(req, res, next) {
   if (req.isAuthenticated()) {
        return next();
    } else {
        res.status(401).json({'serverMessage': 'Access unauthorized', order: req.params.idOrder});
    }
 }

const routeOrder = (app) => {
    
    let controller = app.controllers.order;
    
    app.route('/order/check')
        .post(controller.checkCart);
    
    app.post('/order/pagSeguro/:idOrder', controller.pagSeguro);

    app.get('/order/isOrder/:idOrder', verificaAutenticacao, controller.isOrder);
    
    //app.post('/pagseguro/notificacao', controller.notification)
}

module.exports = routeOrder;