'use strict';

const mail = (app) => {
    
    let controller = app.controllers.mail;
    
    app.post('/pre-lancamento', controller.registerMail);
    
}

module.exports = mail;