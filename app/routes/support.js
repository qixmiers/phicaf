'use strict';

module.exports = (app) => {
    
    let support = app.controllers.support;
    
    app.route('/support') 
        .post(support.save);
    
        
}