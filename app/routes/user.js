'use strict';

function verificaAutenticacao(req, res, next) {
   if (req.isAuthenticated()) {
        return next();
    } else {
        res.status(401).json('Não autorizado');
    }
 }
 
const userRouter = (app) => {
    
    let controller = app.controllers.user;
    
    app.route('/singUp')
        .post(controller.singUp);
    
    app.route('/myOrder')
        .get(verificaAutenticacao, controller.order);

    app.route('/orderDetails/:salesId')
        .get(verificaAutenticacao, controller.orderDetails);
        
    app.route('/perfil')
        .get(verificaAutenticacao, controller.perfil);
    
    app.route('/perfil/password')
        .post(verificaAutenticacao, controller.changePassword);
}

module.exports = userRouter;