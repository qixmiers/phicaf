'use strict';
const multer = require('multer');
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './public/images/')
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + '-' + file.originalname)
  }
});

function verificaAutenticacao(req, res, next) {
   if (req.isAuthenticated()) {
        return next();
    } else {
        res.status(401).json('Não autorizado');
    }
 }



const upload = multer({ storage: storage });


module.exports = (app) => {
    
    let product = app.controllers.product;
    
    app.route('/product/create')
        .post(verificaAutenticacao, upload.fields([{name: 'avatar'}, {name: 'award'}]), product.create);
        
    app.route('/product')
        .get(product.productList);

    app.route('/product/:idProduct')
        .get(product.getProduct)
        .put(upload.fields([{name: 'avatar'}, {name: 'award'}]), product.productUpdate);
    
}