'use strict';

const passport = require('passport');

const routeAuth = (app) => {
    
    let controller = app.controllers.auth;
    
    app.post('/login',
        passport.authenticate('local'),
        controller.session
    );

    app.get('/logout', 
        controller.logout
    );
}

module.exports = routeAuth;