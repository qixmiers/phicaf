'use strict';

module.exports = (app) => {
    
    let address = app.controllers.address;
    
    app.route('/address/shipping/:cepTo')
        .post(address.getPrice)
        .get(address.getAddress);
        
    
}