'use strict';
process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = '0';
const http = require('http');
const app = require('./config/express')();
require('./config/passport')();
require('./config/database.js')('mongodb://localhost/phicafe');

http.createServer(app).listen(app.get('port'), () => {
   console.log('Start server in port '+app.get('port'));
});