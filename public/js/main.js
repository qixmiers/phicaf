'use strict';
var app = angular.module('phicafe', ['ngRoute', 'ngResource', 'ui.utils.masks', 'ngCookies'])
    .config(function($routeProvider, $httpProvider, $locationProvider){
        
        $httpProvider.interceptors.push('phiInterceptor');
        
        $routeProvider
            .when('/',{
                templateUrl: 'partials/homePage.html'         
            })
            .when('/home',{
                templateUrl: 'partials/homePage.html'         
            })
            .when('/produtos',{
                templateUrl: 'partials/product.html',
                controller: 'productsController'        
            })
            .when('/quem-nos-somos', {
                templateUrl: 'partials/whoWeAre.html',
                controller: 'supportController'
            })
            .when('/nossa-historia', {
                templateUrl: 'partials/ourHistory.html',
                controller: 'supportController'
            })
            .when('/comprar-online', {
                templateUrl: 'partials/buyOnline.html',
                controller: 'productsController'
            })
            .when('/produto/:idProduct', {
                templateUrl: 'partials/productSheet.html',
                controller: 'productController'
            })
            .when('/pedido',{
                templateUrl: 'partials/order.html',
                controller: 'cartController'
            })
            .when('/login', {
                templateUrl: 'partials/login.html',
                controller: 'userController'
            })
            .when('/cadastrar', {
                templateUrl: 'partials/register.html',
                controller: 'userController'
            })
            .when('/finalizar/:IDorder', {
                templateUrl: 'partials/formOrder.html',
                controller: 'cartController'
            })
            .when('/user/pedido', {
                templateUrl: 'partials/userOrder.html',
                controller: 'userController'
            })
            .when('/user/pedido/:salesId',{
                templateUrl: 'partials/userOrderDetail.html',
                controller: 'userController'
            })
            .when('/user/perfil', {
                templateUrl: 'partials/perfil.html',
                controller: 'userController'    
            })
            .when('/sales/list', {
                templateUrl: 'partials/sales.html',
                controller: 'salesController',
                controllerAs: 'vm'
            })
            .when('/sales/:salesID', {
                templateUrl: 'partials/salesDetails.html',
                controller: 'salesController',
                controllerAs: 'vm'
            });
            
        $routeProvider.otherwise({redirectTo: '/'});
        
});
