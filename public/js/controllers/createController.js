angular
    .module('phicafe')
    .controller('createController', createController);

createController.$inject = ['createService'];

function createController(createService){
    
    var vm = this;
    vm.postCreateProduct = postCreateProduct;
    vm.postProduct = postProduct;
    vm.product = {
        name: '',
        url: '',
        description: '',
        price: '',
        quantity: '',
        productType: '',
        taste: '',
        foodPairing: '',
        award: [{
            name: '',
            avatar: ''
        }],
        video: '',
        avatar: '',
        classHtml: '',
        weight: '',
        password: ''
    };
    vm.at;

    function postProduct(product){
        postCreateProduct(product);
    }

    function postCreateProduct(data){
        var fd = new FormData();
        fd.append('data', data);
        return createService.postCreateProduct(fd)
            .then(function(result){
                console.log(result);
            });
    }
}