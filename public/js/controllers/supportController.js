'use strict'
angular.module('phicafe').controller('supportController',
    function($scope, Support){
        
        $scope.send = function(){
            $scope.support.$save()
                .then(function(){
                    $scope.message = {text: 'Mensagem enviada com sucesso!'};
                })
                .catch(function(err){
                    $scope.message = {text: 'Não foi possível enviar a mensagem!'};
                })    
        }
        
        $scope.initAboutUs = function(){
            $("body").animate({scrollTop: 135}, "slow");
        }
        
        $scope.supportInit = function(){
            $scope.message = {text: ''};
            $scope.support = new Support();
        }
});