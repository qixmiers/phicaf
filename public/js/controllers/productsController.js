'use strict';

angular.module('phicafe').controller('productsController', 
    function($scope, Product, $location){
        $scope.products  = [];
		$scope.filtro = '';
		
		$scope.mensagem = {texto: ''};
		
		function getAllProduct(){
			$scope.loadingShow();
			if (localStorage.getItem('todosProdutos')) {
				$scope.products = JSON.parse(localStorage.getItem('todosProdutos'));
				$scope.loadingHide();
				if (typeof $scope.isotopeController === 'function') {
					setTimeout($scope.isotopeController, 0);
				}
			}
			Product.query(
				function(products) {
					//$("body").animate({scrollTop: 135}, "slow");
					//$scope.products = products;
					if (!$scope.products || $scope.products.length == 0) {
						$scope.products = products;
					}
					localStorage.setItem('todosProdutos', JSON.stringify(products));
					if(typeof $scope.isotopeController === 'function'){
						setTimeout($scope.isotopeController, 0);
					}
					$scope.loadingHide();
				},
				function(erro) {
					$scope.mensagem = {	
						texto : "Não foi possível obter a lista de produtos"
					};
					$scope.loadingHide();
				}
			);
			
		}
		
	    
        $scope.productInit = function(){
            getAllProduct();
        };

		$scope.loadingHide = function() {
			jQuery(function($){
				$('#loading-page-background').hide();
				
			});
		}
		$scope.loadingShow = function() {
			jQuery(function($){
				$('#loading-page-background').hide();
			});
		}

		$scope.isotopeController = function() {
			jQuery(function($){
				function getHashFilter() {
					// get filter=filterName
					var matches = location.hash.match( /filter=([^&]+)/i );
					var hashFilter = matches && matches[1];
					return hashFilter && decodeURIComponent( hashFilter );
				}

    			var $grid = $('.isotope-grid');

				// bind filter button click
				var $filterButtonGroup = $('.isotope-filters');
				$filterButtonGroup.on( 'click', 'a', function() {
					var filterAttr = $( this ).attr('data-filter');
					// set filter in hash
					window.location.hash = '/produtos#filter=' + encodeURIComponent( filterAttr );
				});

				var isIsotopeInit = false;

				function onHashchange() {
					var hashFilter = getHashFilter();
					if ( !hashFilter && isIsotopeInit ) {
						return;
					}
					isIsotopeInit = true;
					// filter isotope
					$grid.isotope({
						itemSelector: '.isotope-grid-item',
						masonry: {
							columnWidth: '.isotope-grid-sizer',
							gutter: '.isotope-gutter-sizer',
							isFitWidth: true     
							},
						filter: hashFilter
					});
					// set selected class on button
					if ( hashFilter ) {
						$filterButtonGroup.find('.active').removeClass('active');
						var $current = $filterButtonGroup.find('[data-filter="' + hashFilter + '"]');
						$current.parent().addClass('active');
					}
				}

    			$(window).on( 'hashchange', onHashchange );

    // trigger event handler to init Isotope
    			onHashchange();
			});
		}
});
