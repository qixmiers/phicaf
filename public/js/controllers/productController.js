'use strict';
angular.module('phicafe').controller('productController',
    function($scope, $routeParams, Product){
        $scope.typeProduct = [{
            name: 'Grão',
            value: 'grao'
        },
        {
            name: 'Moído',
            value: 'moido'
        }
        ];
        $scope.itemType = $scope.typeProduct[0];
        if($routeParams.idProduct){
            Product.get({idProduct: $routeParams.idProduct},
                function(product){
                    $scope.loadingHinde();
                    $scope.product = product;
                    $scope.priceProduct = product.prices[0].price;
                    $scope.product.price = product.prices[0].price;
                    $scope.product.weight = product.prices[0].weight;
                    $scope.product.weightDescription = product.prices[0].description;
                    $scope.product.typeProduct = $scope.typeProduct[0].name;
                    $scope.item = product.prices[0];
                    $scope.pesoPagSeguro = product.prices[0].weight;
                    $scope.valorPagSeguro = product.prices[0].price*100;
                    if(parseInt(product.prices[0].weight) > 100){
                        $scope.pesoPagSeguro = '0.'+product.prices[0].weight;
                    }
                },
                function(error){
                    $scope.message = {
                        text: 'Não foi possível carregar o produto'
                    }
                    $scope.loadingHinde();
                }
            );
        }
    $scope.loadingHinde = function () {
        jQuery(function($){
            $('#loading-page').hide();
        });
    }

    $scope.weightPrice = function () {
        $scope.priceProduct = $scope.item.price;
        $scope.product.price = $scope.item.price;
        $scope.product.weight = $scope.item.weight;
        $scope.product.weightDescription = $scope.item.description;
        $scope.pesoPagSeguro = $scope.item.weight;
        $scope.valorPagSeguro = $scope.item.price*100;
        if (parseInt($scope.item.weight) > 100 && parseInt($scope.item.weight) < 1000) {
            $scope.pesoPagSeguro = '0.'+$scope.item.weight;
        }else {
            $scope.pesoPagSeguro = ($scope.item.weight/1000).toFixed(3);
        }
    }
    $scope.onChangeTypeProduct = function() {
        $scope.product.typeProduct = $scope.itemType.name;
    }
});