angular.module('phicafe').controller('cartController',
    function($scope, $http, $cookieStore, $location, $routeParams){
        $scope.total = 0;
        $scope.totalWeight = 0;
        $scope.pesoPagSeguro;
        $scope.valorPagSeguro;
        $scope.shippingValue = 0;
        $scope.messageCep = {text: ''};
        $scope.loadingShipping = false;
        $scope.messageCheck = {text: ''};
        //$scope.urlPagSeguro = 'https://sandbox.pagseguro.uol.com.br/v2/checkout/payment.html?code=';
        $scope.urlPagSeguro = 'https://pagseguro.uol.com.br/v2/checkout/payment.html?code=';
        $scope.initForm = function(){
            
            $scope.buyer = {
                'name': '',
                'email': ''
            }
            
            $http({method: 'GET', url: '/order/isOrder/'+$routeParams.IDorder})
                .then(function(isAuthenticatedOrder){
                    if(isAuthenticatedOrder){
                        $scope.buyer = {
                            'name' : '',
                            'email': isAuthenticatedOrder.data.email 
                        }
                    }
                })
                .catch(function(err){
                    if(err.status === 404){
                        alert('Pedido não encontrado!');
                        $location.path('home/');
                        return;
                    }
                });
        }
        
        $scope.addOrder = function(product){
            var cart = [];
            if($cookieStore.get('cart')){
                cart = JSON.parse($cookieStore.get('cart'));
            }
            var pesoDescricao = product.weightDescription;
            var nameProduct = product.name + ' ' + product.typeProduct + ' ' + pesoDescricao;
            if(cart.length > 0){
                for(var i = 0; i < cart.length; i++){
                    if(cart[i]._id == product._id && cart[i].typeProduct == product.typeProduct && cart[i].weight == product.weight){
                        cart[i].quantity = cart[i].quantity + 1;
                        break;
                    }
                    if((i+1) >= cart.length){
                        cart.push({
                            name: nameProduct,
                            description: product.description,
                            quantity: 1,
                            price: product.price,
                            _id: product._id,
                            url: product.url,
                            avatar: product.avatar,
                            weight: product.weight,
                            typeProduct: product.typeProduct,
                            tableWeight: pesoDescricao
                        });
                        break;
                    }
                }
                $cookieStore.put('cart',JSON.stringify(cart));
                return;
            }
            
            cart.push({
                name: nameProduct,
                description: product.description,
                quantity: 1,
                price: product.price,
                _id: product._id,
                url: product.url,
                avatar: product.avatar,
                weight: product.weight,
                typeProduct: product.typeProduct,
                tableWeight: pesoDescricao
            });
            $cookieStore.put('cart',JSON.stringify(cart));
        };
        
        $scope.listProductOrder = 0;
        
        $scope.getTotal = function(){
            var total = 0,
                totalWeight = 0;
            for(var i = 0; i < $scope.listProductOrder.length; i++){
                if(!$scope.listProductOrder[i]){
                    delete $scope.listProductOrder[i];
                    return;
                }
                var product = $scope.listProductOrder[i];
                total += (product.price * product.quantity);
                totalWeight += parseInt(product.weight * product.quantity);
            }
            $scope.total = total;
            if(typeof $scope.shippingOrder !== "undefined"){
                $scope.total = ($scope.total + parseFloat($scope.shippingOrder.Valor));
            }
            
            
            $scope.totalWeight = formatNumber(totalWeight); 
            $scope.pesoPagSeguro = $scope.totalWeight;
            $scope.valorPagSeguro = total*100;
            if(parseInt($scope.totalWeight) > 100){
                $scope.pesoPagSeguro = '0.'+$scope.totalWeight;
            }
            console.log($scope.listProductOrder);
            return $scope.total;
        }
        function formatNumber(num){
            return ("" + num).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, function($1) { return $1 + "." });
        }
        
        $scope.shipping = function(cepTo){
            if(!cepTo || cepTo.length < 8){
                return;
            }
            
            $scope.messageCep = {text: ''};
            $scope.shippingOption = [];
            $scope.loadingShipping = true;
            
            var data = {order: $scope.listProductOrder};
            $http({method: 'POST', url: '/address/shipping/'+cepTo, data})
                .then(function(correios){
                    $scope.shippingOption.push({
                        Erro: correios.data[2],
                        Description: 'Pac',
                        PrazoEntrega: '14',
                        Valor: correios.data[3]
                    });
                    $scope.loadingShipping = false;
                })
                .catch(function(err){
                    if(err.status === 404){
                        $scope.messageCep = {text: 'Cep não encontrado!'};
                    }
                    $scope.loadingShipping = false;
                });
        };
        
        $scope.initOrder = function(){
            $("body").animate({scrollTop: 135}, "slow");
            if($cookieStore.get('cart')){
                $scope.listProductOrder = JSON.parse($cookieStore.get('cart'));
                $scope.getTotal();
            }
        };
        
        $scope.checkShipping = function(key){
            $scope.shippingOrder = $scope.shippingOption[key];
            $scope.getTotal();
        }
        
        $scope.changeQuantity = function(quantity, key){
            if(quantity === 0 || isNaN(quantity)){
                quantity = 1;
            }
            $scope.listProductOrder[key].quantity = parseInt(quantity);
            $scope.updateStorage();
        }
        
        $scope.deleteProduct = function(key){
            delete $scope.listProductOrder[key];
            var cart = [];
            $scope.listProductOrder.forEach(function(product, index){
                cart.push({
                    name: product.name,
                    description: product.description,
                    quantity: 1,
                    price: product.price,
                    _id: product._id,
                    url: product.url,
                    avatar: product.avatar,
                    weight: product.weight,
                    typeProduct: product.typeProduct,
                    tableWeight: product.tableWeight
                });
            });
            $scope.listProductOrder = cart;
            $scope.updateStorage();
        }
        
        $scope.updateStorage = function(){
            $cookieStore.put('cart', JSON.stringify($scope.listProductOrder)); 
            $scope.getTotal();
        }
        
        
        
        $scope.checkOrder = function(){
            var data = {products: JSON.parse($cookieStore.get('cart'))};
            $http({method: 'POST', url: '/order/check', data})
                .then(function(order){
                    if(!order.data.isAuthenticated){
                        window.location.href = '#login?order='+order.data._id;
                        return;
                    }
                    $location.path('finalizar/'+order.data._id);
                    
                })
                .catch(function(err){
                    console.log(err);
                });
            //$scope.messageCheck = {text: ''};
            //$location.path('login');
        };
        
        $scope.address = function(cepTo){
            
            if(!cepTo || cepTo.length < 8){
                return;
            }
            
            $scope.messageCep = {text: ''};
            $scope.loadingShipping = true;
            
            $scope.addressCep = {
                "cep": "",
                "logradouro": "",
                "complemento": "",
                "bairro": "",
                "localidade": "",
                "uf": ""
            };
            $scope.messageAddress = {text: 'buscando...', 'classHtml': 'text-info'};
            $http({method: 'GET', url: '/address/shipping/'+cepTo})
                .then(function(correios){
                    $scope.addressCep = {
                        "cep": correios.data.cep,
                        "logradouro": correios.data.logradouro,
                        "complemento": correios.data.complemento,
                        "bairro": correios.data.bairro,
                        "localidade": correios.data.localidade,
                        "uf": correios.data.uf
                    };
                    $scope.messageAddress = {text: ''};
                })
                .catch(function(err){
                    if(err.status === 404){
                        $scope.messageAddress = {text: 'Cep não encontrado!', 'classHtml': 'text-danger'};
                    }
                });
        };
        
        $scope.pagSeguro = function(buyer, address){
            $scope.btndisabled = true;
            $scope.message = {
                textClass: 'text-warning',
                text: 'Enviando...'
            };
            var data = { 
                cep: address.cep,
                street: address.logradouro,
                complemento: address.complemento,
                district: address.bairro,
                homeNumber: address.numero,
                nameBuyer: buyer.name,
                emailBuyer: buyer.email,
                phoneAreaCode: buyer.phoneNumber.slice(0,2),
                phoneNumber: buyer.phoneNumber.slice(2),
                city: address.localidade,
                state: address.uf
            };
            
            $http({method: 'POST', url: '/order/pagSeguro/'+$routeParams.IDorder, data})
                .then(function(pagamento){
                    if(pagamento.data.checkout.code){
                        $cookieStore.remove('cart');
                        $scope.message = {
                            textClass: 'text-success',
                            text: 'Redirecionando para o PagSeguro...'
                        };
                        window.location.href = $scope.urlPagSeguro+pagamento.data.checkout.code[0];
                    }
                })
                .catch(function(err){
                    $scope.message = {
                        textClass: 'text-danger',
                        text: 'Desculpe, mas ocorreu um erro interno, por favor entre com contato com o suporte'
                    };
                });
                
        }

    }
)