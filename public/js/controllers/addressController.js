angular.module('phicafe').controller('addressController', 
    function ($scope, $http) {
        
        $scope.shipping = function(){
            var query = '?cepTo='+$scope.cepTo+'&weightPackage=20&lengthPackage=20&heightPackage=4&widthPackage=11&diameterPackage=20';
            $http({method: 'GET', url: '/address/shipping'+query})
                .then(function(correios){
                    $scope.shippingOption = correios.data;
                })
                .catch(function(err){
                    console.log(err.data.serverMessage);
                });
        }
        
        
    
});
