'use strict';

angular
    .module('phicafe')
    .controller('salesController', salesController);

salesController.$inject = ['salesService', '$routeParams'];

function salesController(salesService, $routeParams){
    
    var vm = this;
    vm.init = init;
    vm.getAllSales = getAllSales;
    vm.Allsales = [];
    vm.detailSales = [];
    vm.initDetailSales = initDetailSales;
    
    function init(){
        return getAllSales();
    }

    function getAllSales(){
        return salesService.getAllSales()
            .then(function(response){
                if(!response.status){
                    return vm.Allsales = response;
                }
                return response;
            });
    }

    function initDetailSales(){
        var id = $routeParams.salesID;
        getDetailSales(id).then(function(){
        });
    }

    function getDetailSales(id){
        return salesService.getDetailSales(id)
            .then(function(response){
                if(response.length > 0){
                    return vm.detailSales = response[0];
                }
            });
    }


}