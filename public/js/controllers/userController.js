app.controller('userController', function($scope, $http, $location, $routeParams){
    $scope.errorPassword = {};
    $scope.login = function(credentials){
        
        $scope.btnDisabled = true;
        
        var data = {
            email : credentials.email,
            password: credentials.password
        };
        $scope.message = {
            text: 'Aguarde...',
            textClass: 'text-info'
        };
        
        $http({method: 'POST', url: '/login'+$scope.query, data})
                .then(function(user){
                    $scope.message = {
                        text: 'Redirecionando...',
                        textClass: 'text-success'
                    };
                    if($scope.query){
                        window.location.href = '#finalizar/'+$location.search().order;
                        return;
                    }
                    $location.path('user/pedido');
                    $scope.btnDisabled = false;
                })
                .catch(function(err){
                    console.log($scope.order);
                    if(err.status === 401){
                        $scope.message = {
                            text: 'Email ou senha inválidos',
                            textClass: 'text-danger'
                        };
                    }
                    $scope.btnDisabled = false;
                });
    };
    
    $scope.singUp = function(userSingUp){
        
        if(userSingUp.password != userSingUp.passwordTwo){
            alert('Senhas diferente');
            return;
        }
        $scope.message = {
            text: 'Aguarde...',
            textClass: 'text-info'
        };
        var data = {
            firstName: $scope.userSingUp.firstName,
            lastName: $scope.userSingUp.lastName,
            email: $scope.userSingUp.email,
            userName: $scope.userSingUp.email.split('@')[0],
            password: $scope.userSingUp.password
        };
        $http({method: 'POST', url: '/singUp', data})
            .then(function(success){
                    $scope.message = {
                        text: 'Cadastrado com sucesso',
                        textClass: 'text-success'
                    };
                    window.location.href = '#login'+$scope.query;
                })
                .catch(function(err){
                    if(err.status === 409){
                            $scope.message = {
                                text: 'Email já cadastrado',
                                textClass: 'text-danger'
                            };
                        return;
                    }
                    $scope.message = {
                        text: 'Não foi possível cadastrar, entre em contato com o suporte',
                        textClass: 'text-danger'
                    };
                });
    }
    
    $scope.myOrder = function(){
        $scope.listOrder = [];
        $("body").animate({scrollTop: 135}, "slow");
        $scope.isLoading = true;
        $http({method: 'GET', url: '/myOrder'})
            .then(function(orders){
                    $scope.isLoading = false;
                    orders.data.forEach(function(order){
                        $scope.listOrder.push(order);
                    });
                })
                .catch(function(err){
                    console.error(err);
                    $scope.isLoading = false;
                });
    }
    
    $scope.userOrderDetail = function(){
        $scope.detailSales = [];
        $("body").animate({scrollTop: 135}, "slow");
        $scope.isLoading = true;
        $http({method: 'GET', url: '/orderDetails/'+$routeParams.salesId})
            .then(function(orders){
                $scope.isLoading = false;
                $scope.detailSales = orders.data[0];
            })
            .catch(function(err){
                console.error(err);
                $scope.isLoading = false;
            });
    }

    $scope.myPerfil = function(){
        $scope.isLoading = true;
        $http({method: 'GET', url: '/perfil'})
            .then(function(perfil){
                    $scope.isLoading = false;
                    $scope.perfil = {
                        'firstName': perfil.data.firstName,
                        'lastName': perfil.data.lastName,
                        'email': perfil.data.email
                    }
                })
                .catch(function(err){
                    console.error(err);
                    $scope.isLoading = false;
                });
    }
    
    
    $scope.changePassword = function(passwords){
        if(passwords.password === ''){
            $scope.errorPassword.password = 'Não pode ser vazio';
            return;
        }
        if(passwords.newPassword === ''){
            $scope.errorPassword.newPassword = 'Não pode ser vazio';
            return;
        }
        if(passwords.repeatNewPassword === ''){
            $scope.errorPassword.repeatNewPassword = 'Não pode ser vazio';
            return;
        }
        
        if(passwords.newPassword !== passwords.repeatNewPassword){
            $scope.errorPassword.text = 'As senhas precisam ser iguais!';
            $scope.errorPassword.textClass = 'text-danger';
            return;
        }
        
        $scope.errorPassword.text = 'Aguarde...';
        $scope.errorPassword.textClass = 'text-info';
        var data = {
            password: passwords.password,
            newPassword: passwords.newPassword,
            repeatNewPassword: passwords.repeatNewPassword
        };
        $http({method: 'POST', url: '/perfil/password', data})
            .then(function(changed){
                    if(changed.status === 200){
                        $scope.passwords.password = '';
                        $scope.passwords.newPassword = '';
                        $scope.passwords.repeatNewPassword = '';
                        $scope.errorPassword.text = 'Senha alterado com sucesso!';
                        $scope.errorPassword.textClass = 'text-success';
                        return;
                    }
                    $scope.errorPassword.text = 'Não foi possível alterar a senha';
                    $scope.errorPassword.textClass = 'text-warning';
                })
                .catch(function(err){
                    if(err.status === 409){
                        $scope.errorPassword.text = 'Senha atual invalida';
                        $scope.errorPassword.textClass = 'text-danger';
                        return;
                    }
                    $scope.errorPassword.text = 'Ocorreu um erro interno, entre em contato com o suporte';
                    $scope.errorPassword.textClass = 'text-danger';
                });
        
    }
    
    $scope.userInit = function(){
        
        $("body").animate({scrollTop: 135}, "slow");
        $scope.query = '';
        $scope.isLoading = false;
        
        if($location.search().order){
            $scope.query = '?order='+$location.search().order
        }
        
        $scope.btnDisabled = false;
        
        $scope.message = {
            text: '',
            textClass: ''
        };
        $scope.credentials = {
            email: '',
            password: ''
        };
        
        $scope.userSingUp = {
            firstName: '',
            lastName: '',
            email: '',
            userName: '',
            password: '',
            passwordTwo: ''
        };
        
        $scope.perfil = {
                        'firstName': '',
                        'lastName': '',
                        'email': ''
                    }
        
        
        $scope.passwords = {
        'password': '',
        'newPassword': '',
        'repeatNewPassword': '',
        };
        $scope.errorPassword = {
            'password': '',
            'newPassword': '',
            'repeatNewPassword': '',
            'textClass': '',
            'text': ''
        };
    }
    
})