app
    .directive('fileModel', fileModel);

fileModel.$inject = ['$parse'];
function fileModel($parse){
    
    return{
        restrict: 'A',
        link: fileModelLink
    }

    function fileModelLink(scope, element, attrs){
        var model = $parse(attrs.fileModel);
        var modelSetter = model.assign;
        element.bind('change', fileModelLinkOnChange);

        function fileModelLinkOnChange(){
            scope.$apply(function(){
                modelSetter(scope, element[0].files[0]);
            });
        }
    }
}