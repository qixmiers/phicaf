app
    .factory('createService', createService);

createService.$inject = ['$http'];

function createService($http){
    return {
        postCreateProduct: postCreateProduct
    };

    function postCreateProduct(data){
        return $http.post('/product/create', data, {
            transformRequest: angular.identity, 
            headers: {
                'Content-Type': undefined
            }
            })
            .then(postCreateProductSuccess)
            .catch(postCreateProductError);
        
        function postCreateProductSuccess(response){
            return response.data;
        }
        function postCreateProductError(error){
            return error;
        }
    }
}