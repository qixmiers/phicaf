app
    .factory('salesService', salesService);

salesService.$inject = ['$http'];

function salesService($http){
    return {
        getAllSales: getAllSales,
        getDetailSales: getDetailSales
    }

    function getAllSales(){
        return $http.get('/sales/all')
            .then(getAllSalesSuccess)
            .catch(getAllSalesError)
        
        function getAllSalesSuccess(response){
            return response.data;
        }
        function getAllSalesError(error){
            return error;
        }
    }

    function getDetailSales(id){
        return $http.get('/sales/'+id)
            .then(getDetailSalesSuccess)
            .catch(getDetailSalesError);

        function getDetailSalesSuccess(response){
            return response.data;
        }
        function getDetailSalesError(error){
            return error;
        }
    }
}
    