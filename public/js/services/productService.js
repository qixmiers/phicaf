app.factory('Product',
    function($resource) {
    	return $resource('/product/:idProduct');
});