app.factory('phiInterceptor', function($location, $q){
    var interceptor = {
        
        responseError: function(response){
            
            if(response.status === 401){
                
                var url = response.config.url.split("=");
                var query = {};
                
                if(url[1]){
                    
                    query = {'order': url[1]};
                    
                }else
                    if(response.data.order){
                        
                        query = {'order': response.data.order};
                        
                }
                
                $location.path('/login').search(query);
                
            }
            
            return $q.reject(response);
        }
        
    }
    
    return interceptor;
    
});